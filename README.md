girls-htmlmail
==============

マキアの仕様に最適化されたHTMLメールのテンプレート


## 機能

- 互換性の高いメールテンプレート
- メール作成補助のタスクランナー


## 使い方

1. [node.js](https://nodejs.org/)をインストール
2. girls-htmlmailを[ダウンロード](https://bitbucket.org/mtt0988610576/girls-htmlmail/downloads)
3. 展開されたgirls-htmlmailフォルダ内でコマンドプロンプトを開き、`gulp`を実行
4. srcフォルダ内のhtmlで作業
5. dstフォルダ内に自動的に完成htmlが吐き出されるので、そいつをマキアに張りつけて送信


## マキアルール

マキアルールに関しては[Wiki](https://bitbucket.org/mtt0988610576/girls-htmlmail/wiki/Home)を参照


## ライブラリ

- [Cerberus](http://tedgoas.github.io/Cerberus/)