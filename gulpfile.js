'use strict';

var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var $ = gulpLoadPlugins();

var path = {
	'template': {
		'src': './_src/**/*.html',
		'dst': './_dst/'
	},
	'style': {
		'src': './_src/**/*.{sass,scss}',
		'dst': './_src/'
	}
}

// Build HtmlMail Template
gulp.task('build', function() {
	gulp.src(path.template.src)
		.pipe($.changed(path.template.dst))
		.pipe($.plumber())
		.pipe($.inlineCss())
		.pipe($.removeCode({ production: true }))
		.pipe($.replace(/class=".+?"/g, ''))
		.pipe($.replace('rel="table"', 'cellpadding="0" cellspacing="0" border="0" width="100%" align="center"'))
		.pipe($.replace('http://girls-chat.tv', '%%12%%'))
		.pipe($.replace('%link%', '%%12%%/user/start.aspx?loginid=%%2%%&password=%%3%%&goto=DisplayDoc.aspx&doc'))
		.pipe(gulp.dest(path.template.dst))
		.pipe($.size({title: 'templates'}));
});

// Sass Compile to Css
gulp.task('sass', function() {
	gulp.src(path.style.src)
		.pipe($.plumber())
		.pipe($.sass().on('error', $.sass.logError))
		.pipe(gulp.dest(path.style.dst))
		.pipe($.size({title: 'sass'}));
});

gulp.task('watch', function() {
	gulp.watch(path.template.src, function() {
		gulp.start('build');
	});
	gulp.watch(path.style.src, function() {
		gulp.start('sass');
	});
});

gulp.task('default', ['watch']);